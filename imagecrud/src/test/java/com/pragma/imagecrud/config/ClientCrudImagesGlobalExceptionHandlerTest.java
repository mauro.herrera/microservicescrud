package com.pragma.imagecrud.config;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = {ClientCrudImagesGlobalExceptionHandler.class})
class ClientCrudImagesGlobalExceptionHandlerTest {

    @Autowired
    private ClientCrudImagesGlobalExceptionHandler errorHandler;

    @Test
    void handleCityNotFoundExceptionTest() {
        assertEquals(HttpStatus.NOT_FOUND, errorHandler.handleClientNotFoundException().getStatusCode());
    }

    @Test
    void handleNoDataFoundExceptionTest() {
        assertEquals(HttpStatus.NOT_FOUND, errorHandler.handleNoDataFoundException().getStatusCode());
    }

    @Test
    void handleHttpMessageNotReadableExceptionTest() {
        assertEquals(HttpStatus.BAD_REQUEST, errorHandler.handleHttpMessageNotReadableException().getStatusCode());
    }
}
