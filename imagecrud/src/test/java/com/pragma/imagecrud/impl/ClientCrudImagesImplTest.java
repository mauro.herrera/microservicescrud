package com.pragma.imagecrud.impl;

import com.pragma.imagecrud.dto.ClientImage;
import com.pragma.imagecrud.exceptions.ClientNotFoundException;
import com.pragma.imagecrud.interfaces.IClientCrudImagesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = {ClientCrudImagesImpl.class})
class ClientCrudImagesImplTest {

    @Autowired
    private ClientCrudImagesImpl clientCrudImages;

    @MockBean
    private IClientCrudImagesRepository clientCrudImagesRepository;

    ClientImage clientImage = new ClientImage("TEST", "photoBase64");

    @Test
    void createClientTest() {
        when(clientCrudImagesRepository.save(any(ClientImage.class))).thenReturn(clientImage);

        assertEquals(clientImage, clientCrudImages.createClient(clientImage));
    }

    @Test
    void getClientByIdTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.of(clientImage));

        assertEquals(clientImage, clientCrudImages.getClientById("TEST"));
    }

    @Test
    void getAllClientsTest() {
        List<String> ids = List.of("TEST");
        when(clientCrudImagesRepository.findByIdsIn(ids)).thenReturn(List.of(clientImage));

        assertEquals(List.of(clientImage), clientCrudImages.getAllClients(ids));
    }

    @Test
    void updateClientTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.of(clientImage));
        when(clientCrudImagesRepository.save(any(ClientImage.class))).thenReturn(clientImage);

        assertEquals(clientImage, clientCrudImages.updateClient("TEST", clientImage));
    }

    @Test
    void deleteClientTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.of(clientImage));

        clientCrudImages.deleteClient("TEST");

        verify(clientCrudImagesRepository, times(1)).delete(any(ClientImage.class));
    }

    @Test
    void getClientByIdFailTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> clientCrudImages.getClientById("TEST"));
    }

    @Test
    void updateClientFailTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> clientCrudImages.updateClient("TEST", clientImage));
    }

    @Test
    void deleteClientFailTest() {
        when(clientCrudImagesRepository.findById(anyString())).thenReturn(Optional.empty());

        assertThrows(ClientNotFoundException.class, () -> clientCrudImages.deleteClient("TEST"));
    }
}
