package com.pragma.imagecrud.controller;

import com.pragma.imagecrud.dto.ClientImage;
import com.pragma.imagecrud.interfaces.IClientCrudImagesImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = { ClientCrudImagesController.class})
class ClientCrudImagesControllerTest {

    @Autowired
    private ClientCrudImagesController clientCrudImagesController;

    @MockBean
    private IClientCrudImagesImpl clientCrudImagesImpl;

    ClientImage clientImage = new ClientImage("TEST", "photoBase64");

    @Test
    void createClientTest() {
        when(clientCrudImagesImpl.createClient(any(ClientImage.class))).thenReturn(clientImage);

        assertEquals(HttpStatus.OK, clientCrudImagesController.createClient(clientImage).getStatusCode());
    }

    @Test
    void getClientByIdTest() {
        when(clientCrudImagesImpl.getClientById(anyString())).thenReturn(clientImage);

        assertEquals(HttpStatus.OK, clientCrudImagesController.getClientById("TEST").getStatusCode());
    }

    @Test
    void getAllClientsTest() {
        when(clientCrudImagesImpl.getAllClients(anyList())).thenReturn(List.of(clientImage));

        assertEquals(HttpStatus.OK, clientCrudImagesController.getAllClients(List.of("TEST")).getStatusCode());
    }

    @Test
    void updateClientTest() {
        when(clientCrudImagesImpl.updateClient(anyString(), any(ClientImage.class))).thenReturn(clientImage);

        assertEquals(HttpStatus.OK, clientCrudImagesController.updateClient("TEST", clientImage).getStatusCode());
    }

    @Test
    void deleteClientTest() {
        assertEquals(HttpStatus.OK, clientCrudImagesController.deleteClient("TEST").getStatusCode());
    }
}
