package com.pragma.imagecrud.controller;

import com.pragma.imagecrud.dto.ClientImage;
import com.pragma.imagecrud.interfaces.IClientCrudImagesImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClientCrudImagesController {

    @Autowired
    private IClientCrudImagesImpl clientCrudImagesImpl;

    //C
    @PostMapping(path = "/createClient", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientImage> createClient(@RequestBody ClientImage client) {

        return new ResponseEntity<>(clientCrudImagesImpl.createClient(client), HttpStatus.OK);
    }

    //R
    @GetMapping(path = "/getClient/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientImage> getClientById(@PathVariable("id") String id) {

        return new ResponseEntity<>(clientCrudImagesImpl.getClientById(id), HttpStatus.OK);
    }

    @PostMapping(path = "/getClient", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ClientImage>> getAllClients(@RequestBody List<String> ids) {

        return new ResponseEntity<>(clientCrudImagesImpl.getAllClients(ids), HttpStatus.OK);
    }

    //U
    @PatchMapping(path = "/updateClient/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ClientImage> updateClient(@PathVariable("id") String id, @RequestBody ClientImage client) {

        return new ResponseEntity<>(clientCrudImagesImpl.updateClient(id, client), HttpStatus.OK);
    }

    //D
    @DeleteMapping(path = "/deleteClient/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> deleteClient(@PathVariable("id") String id) {

        clientCrudImagesImpl.deleteClient(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
