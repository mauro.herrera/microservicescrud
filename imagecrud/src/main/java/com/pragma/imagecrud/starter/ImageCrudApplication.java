package com.pragma.imagecrud.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableDiscoveryClient
@EntityScan("com.pragma.imagecrud.dto")
@ComponentScan("com.pragma.imagecrud.*")
@EnableMongoRepositories("com.pragma.imagecrud.interfaces")
public class ImageCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImageCrudApplication.class, args);
	}
}

