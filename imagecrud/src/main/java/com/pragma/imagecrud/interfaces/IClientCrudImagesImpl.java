package com.pragma.imagecrud.interfaces;

import com.pragma.imagecrud.dto.ClientImage;

import java.util.List;

public interface IClientCrudImagesImpl {
    ClientImage createClient(ClientImage client);
    ClientImage getClientById(String id);
    List<ClientImage> getAllClients(List<String> ids);
    ClientImage updateClient(String id, ClientImage client);
    void deleteClient(String id);
}
