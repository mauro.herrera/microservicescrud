package com.pragma.imagecrud.exceptions;

public class NoDataException extends RuntimeException{
    public NoDataException() {
        super("No data found");
    }
}
