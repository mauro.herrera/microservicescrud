package com.pragma.imagecrud.impl;

import com.pragma.imagecrud.dto.ClientImage;
import com.pragma.imagecrud.exceptions.ClientNotFoundException;
import com.pragma.imagecrud.exceptions.NoDataException;
import com.pragma.imagecrud.interfaces.IClientCrudImagesImpl;
import com.pragma.imagecrud.interfaces.IClientCrudImagesRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ClientCrudImagesImpl implements IClientCrudImagesImpl {

    @Autowired
    private IClientCrudImagesRepository clientCrudImagesRepository;

    @Override
    public ClientImage createClient(ClientImage clientImage) {
        clientCrudImagesRepository.save(clientImage);

        log.info("Client created successfully...");

        return clientImage;
    }

    @Override
    public ClientImage getClientById(String id) {
        ClientImage clientImage = clientCrudImagesRepository.findById(id).orElse(null);

        if (clientImage == null) {
            log.info("No client found for client id: {}", id);
            throw new ClientNotFoundException(id);
        } else
            log.info("Client found successfully...");

        return clientImage;
    }

    @Override
    public List<ClientImage> getAllClients(List<String> ids) {
        List<ClientImage> clientImages = clientCrudImagesRepository.findByIdsIn(ids);

        log.info("Clients found successfully...");

        return clientImages;
    }

    @Override
    public ClientImage updateClient(String id, ClientImage client) {
        ClientImage clientImage = clientCrudImagesRepository.findById(id).orElse(null);

        if (clientImage != null) {
            clientCrudImagesRepository.save(client);

            log.info("Client updated successfully...");
        } else {
            log.info("No data found for client id: {}", id);
            throw new ClientNotFoundException(id);
        }

        return client;
    }

    @Override
    public void deleteClient(String id) {
        ClientImage clientImage = clientCrudImagesRepository.findById(id).orElse(null);

        if (clientImage != null) {
            clientCrudImagesRepository.delete(clientImage);

            log.info("Client deleted successfully...");
        } else {
            log.info("No data found for client id: {}", id);
            throw new ClientNotFoundException(id);
        }
    }
}
